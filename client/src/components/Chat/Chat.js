import React, { Component } from 'react';
// import { ReactDOM } from 'react-dom';


//CSS
import './Chat.css';

class Chat extends Component {
  state = { theirMessages: [], messages: [], message: '' }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    //console.log(this.state.message)
  }

  handleSend = (e) => {
    this.setState({ messages: [...this.state.messages, {msg: this.state.message, from:'me'}]})
    // let newMessage = this.state.message
    console.log(this.state)
  }

  handleReceive = (e) => {
    this.setState({ messages: [...this.state.messages, {msg: this.state.message, from:'them'}]})
  }

  render() {
    return (
      <div className="Chat">
        <div className="chatOut">
          { this.state.messages.length > 0 && this.state.messages.map((message, i) => 
            //You should make this a box that displays most recent messages but the box is a set size and doesn't scroll
            <div className={message.from} key={i}>{message.msg}</div> )
          } 
        </div>
        <div className="bottomBar">
          <input className="chatTyping" type="text" ref="textBox" name="message" value={this.state.message} onChange={this.handleChange} />
          <button id="sendChat" onClick={this.handleSend}>Chat</button>
          <button id="receiveChat" onClick={this.handleReceive}>Chat</button>
        </div>
      </div>
    )
  }
}

export default Chat;