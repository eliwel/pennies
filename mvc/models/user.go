package "models"

import(
	"github.com/gorilla/schema"
)

type User struct {
	Username string
	Messages []Message
}
