package web

func (w *Wave) reader() {
  buf := bufio.NewReader(w.conn)

  for {
    pkt, _ := readPacket(buf)
    w.handle(pkt)
  }
}
