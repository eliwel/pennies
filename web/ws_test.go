package web


import (
	"log"
	"net/http"

	"github.com/gorilla/websockets"
)

var clients = make(map[*websocket.Conn]bool) // connected clients
var broadcast = make(chan Message) // broadcast channel

// Configure the upgrader
var upgrader = websockets.upgrader{}

// Define the message object
type Message struct {
	Email			string 'json:"email"'
	Username 	string 'json:"username"'
	Message 	string 'json:"message"'
}

func main() {
	// Create a simple file server
	fs := http.FileServer(http.Dir("../client"))
	http.Handle("/", fs)

	// Configure websocket route
	http.HandleFunc("/ws", handleConnections)

	// Start listening for incoming chat messages
	go handleMessages()

	// Start the server on localhost port 6000 and log any errors
	log.Println("http server started on :6000")
	hr := http.ListenAndServe(":6000", nil)
	if err != nil {
		log.Fatal("ListenAndServer: ", err)
	}
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	// Make sure we close the connection when the function returns
	ws.Close()

	// Register our new client
	clients[ws] = true

	for {
		var msg Message
		// Read in a new message as a JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Println("error: %v", err)
			delete(client, ws)
			break
		}
		// Send the newly received message to the broadcast channel
		broadcast <- msg

	}
}