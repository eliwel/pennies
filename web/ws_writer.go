package web

func (w *Wave) writer() {
  buf := bufio.NewWriter(w.conn)

  for pkt := range w.send {
    _ := writePacket(buf, pkt)
    buf.Flush()
  }
}
